package cz.sapho.pig.latin;

import java.util.Arrays;
import java.util.stream.Collectors;

import static java.lang.Character.isLetter;
import static java.lang.Character.isUpperCase;

public class PigLatinTranslator {
    private static final String CONSONANT_SUFFIX = "ay";
    private static final String VOWELS = "aeiou";
    private static final char WORD_SEPARATOR_CHAR = '-';
    private static final String WORD_SEPARATOR = String.valueOf(WORD_SEPARATOR_CHAR);
    private static final String NO_MODIFICATION_REQUIRED_SUFFIX = "way";
    private static final String NOT_LETTER_OR_HYPHEN = "[^a-z-]";

    public static String translate(String input) {
        return restorePunctuationAndCapitalization(input, modifyInput(input));
    }

    private static String restorePunctuationAndCapitalization(String originalInput, String modifiedInput) {
        String lowerCaseWithoutNonWordCharacters = modifiedInput.toLowerCase().replaceAll(NOT_LETTER_OR_HYPHEN, "");
        StringBuilder restored = new StringBuilder(lowerCaseWithoutNonWordCharacters);

        int originalLength = originalInput.length();
        for (int i = originalLength - 1; i >= 0; i--) {
            char c = originalInput.charAt(i);
            //scala-like pattern matching would look better here
            if (c == WORD_SEPARATOR_CHAR) {
                continue;
            }
            int oneBasedIndex = i + 1;
            if (isUpperCase(c)) {
                restored.replace(i, oneBasedIndex, String.valueOf(restored.charAt(i)).toUpperCase());
                continue;
            }
            if (!isLetter(c)) {
                int offset = oneBasedIndex - originalLength;
                restored.insert(restored.length() + offset, c);
            }
        }
        return restored.toString();
    }

    private static String modifyInput(String input) {
        if (isModificationIsNotRequired(input)) {
            return input;
        }
        if (input.contains(WORD_SEPARATOR)) {
            return modifyEachWordSeparately(input);
        }

        return modifyBasedOnFirstCharacter(input);
    }

    private static String modifyEachWordSeparately(String word) {
        return Arrays.stream(word.split(WORD_SEPARATOR, -1))
                .map(PigLatinTranslator::modifyInput)
                .collect(Collectors.joining(WORD_SEPARATOR));
    }

    private static String modifyBasedOnFirstCharacter(String word) {
        char firstChar = word.toLowerCase().charAt(0);
        if (!isLetter(firstChar)) {
            throw new IllegalStateException("Only letters are expected at the beginning of the word " + word);
        }

        if (VOWELS.contains(String.valueOf(firstChar))) {
            return word + NO_MODIFICATION_REQUIRED_SUFFIX;
        }
        return word.substring(1) + firstChar + CONSONANT_SUFFIX;
    }

    private static boolean isModificationIsNotRequired(String input) {
        boolean endsWithNoModificationsRequiredSuffix = input.toLowerCase().endsWith(NO_MODIFICATION_REQUIRED_SUFFIX);
        return endsWithNoModificationsRequiredSuffix || input.isEmpty();
    }
}
