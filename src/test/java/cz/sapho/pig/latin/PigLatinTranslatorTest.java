package cz.sapho.pig.latin;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PigLatinTranslatorTest {
    @ParameterizedTest(name = "{2}")
    @MethodSource("provideTestData")
    void translate(String originalString, String expectedResult, String message) {
        assertEquals(expectedResult, PigLatinTranslator.translate(originalString), message);
    }

    @Test
    void expectExceptionWhenAWordStartsWithNonLetterCharacter() {
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> PigLatinTranslator.translate("2b"));
        assertEquals("Only letters are expected at the beginning of the word 2b", exception.getMessage());
    }

    private static Stream<Arguments> provideTestData() {
        return Stream.of(
                Arguments.of("Pig-Latin", "IgpaY-atinlay", "pig latin"),
                Arguments.of("Hello", "Ellohay", "consonant example"),
                Arguments.of("apple", "appleway", "vowel example"),
                Arguments.of("stairway", "stairway", "non modified example"),
                Arguments.of("can’t", "antca’y", "punctuation 1 example"),
                Arguments.of("end.", "endway.", "punctuation 2 example"),
                Arguments.of("this-thing", "histay-hingtay", "hyphen example"),
                Arguments.of("Beach", "Eachbay", "capitalization 1 example"),
                Arguments.of("McCloud", "CcLoudmay", "capitalization 2 example"),
                Arguments.of("", "", "empty string"),
                Arguments.of("-", "-", "only hyphen"),
                Arguments.of("-A", "-Away", "start with hyphen"),
                Arguments.of("myWaY", "myWaY", "way a.k.a 'no change required' is case insensitive"),
                Arguments.of("eNd.-bE", "eNdway-e.bay", "capitalization is skipped if need to capitalize punctuation"),
                Arguments.of("sTairWay-eNd.-BEnd", "sTairWay-eNdwaY-e.ndbay", "non modified, hyphened with punctuation and capitalization example")
        );
    }

}
